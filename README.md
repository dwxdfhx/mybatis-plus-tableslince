##mybatis | mybatis plus 自动分表插件 
###已支持自动分表规则    
1.按年    
2.按月    
3.按日    
规则目录:package com.mybatis.plug.slicingStrategy;可以在这里自定义分表时间策略  

###maven引入  
```xml
<dependency>
       <groupId>com.mybatis.plug</groupId>
       <artifactId>mybatis-plus-tableslince</artifactId>
       <version>1.0-SNAPSHOT</version>
</dependency>
```

###spring环境配置   
spring xml中新增: 
```xml
 <!-- ApplicationContext 改工具类一般都会在自己业务系统中有,可以用自己的-->
    <bean class="com.mybatis.plug.util.ApplicationContextUtil"></bean>
    
 <!-- 切割表策略manager 配置策略-->
    <bean id="strategyManager" class="com.mybatis.plug.interceptor.StrategyManager">
        <property name="strategies">
            <map>
                <entry key="YEAR" value="com.mybatis.plug.slicingStrategy.ByYearStrategy"/>
                <entry key="MOUTH" value="com.mybatis.plug.slicingStrategy.ByMouthStrategy"/>
                <entry key="DAY" value="com.mybatis.plug.slicingStrategy.ByDayStrategy"/>
            </map>
        </property>
        <property name="slicingTables">
            <map>
                <entry key="SYS_SYSTEM_LOG" value="cn.buz.models.entity.AuthLog"/>
                <entry key="SYS_LOG4J_RECORDS" value="cn.buz.models.entity.Log4jRecords"/>
            </map>
        </property>
    </bean>
```   
spring.xml中修改mybatis plus的配置:
```xml
<!--插件-->
        <property name="plugins">
            <array>
                <!-- 分页插件配置 -->
                <bean id="paginationInterceptor"
                      class="com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor"/>
                <!-- 乐观锁插件 -->
                <bean id="optimisticLockerInterceptor"
                      class="com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor"/>
                <bean id="tableSlicingInterceptor"
                      class="com.mybatis.plug.interceptor.TableSlicingInterceptor"/>
            </array>
        </property>
```

被分表的entity上添加注解:  
@TableSlicing(slicing = true, value = "SYS_AUTH_LOG", strategy = "DAY")  

###spring boot配置    
mybatis plus 的@Configuration配置文件中新增:    
```
    /**
     * 分表插件
     *
     * @return
     */
    @Bean("strategyManager")
    public StrategyManager strategyManager() {
        StrategyManager strategyManager = new StrategyManager();
        Map<String, String> strategies = new HashMap<>();
        strategies.put("YEAR", "com.mybatis.plug.slicingStrategy.ByYearStrategy");
        strategies.put("MOUTH", "com.mybatis.plug.slicingStrategy..ByMouthStrategy");
        strategies.put("DAY", "com.mybatis.plug.slicingStrategy..ByDayStrategy");
        strategyManager.setStrategies(strategies);
        Map<String, String> slicingTables = new HashMap<>();
        slicingTables.put("SYSTEM_AUTH_LOG", "cn.buz.models.entity.AuthLog");
        slicingTables.put("SYSTEM_LOG4J_RECORDS", "cn.buz.models.entity.Log4jRecords");
        strategyManager.setSlicingTables(slicingTables);
        return strategyManager;
    }
  /**
     * table分表插件
     */
    public TableSlicingInterceptor tableSlicingInterceptor() {
        TableSlicingInterceptor slicing = new TableSlicingInterceptor();
        return slicing;
    }    
```

被分表的entity上添加注解:  
@TableSlicing(slicing = true, value = "SYS_AUTH_LOG", strategy = "DAY")  
    
###其他说明     
1.目前只在oracle环境下测试过,没有兼容多数据库类型.  
