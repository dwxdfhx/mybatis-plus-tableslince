package com.mybatis.plug.util;

import cn.hutool.core.util.StrUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by
 * sql字符串处理
 *
 * @author dw
 * @date 2018/10/26
 */
public class SqlStringUtil {

    public static boolean isSelectSql(String sql) {
        String opertype = getOperation(sql);
        if (!StrUtil.equals(opertype, "SELECT".toUpperCase())) {
            return false;
        } else {
            return true;
        }
    }

    public static String getOperation(String sql) {
        String upperCaseSql = sql.toUpperCase();
        String opertype = upperCaseSql.substring(0, upperCaseSql.indexOf(" "));
        return opertype;
    }

    public static String getTableName(String sql) {
        String tableName = null;
        sql = StrUtil.removeAll(sql, "\n");
        sql = sql.toUpperCase();
        String oper = sql.substring(0, sql.indexOf(" "));

        if (StrUtil.containsAnyIgnoreCase(oper, "delete".toUpperCase(), "insert".toUpperCase())) {
            String[] words = sql.split(" ");
            tableName = words[2];
        } else if (StrUtil.containsAnyIgnoreCase(oper, "select".toUpperCase())) {
            tableName = sql.split(" FROM ")[1];
            if (StrUtil.equalsIgnoreCase(tableName, "dual")) {
                return tableName;
            }
            tableName = tableName.substring(0, tableName.indexOf(" "));
        } else if (StrUtil.containsAnyIgnoreCase(oper, "update".toUpperCase())) {
            String[] words = sql.split(" ");
            tableName = words[1];
        }
        return tableName;
    }

    /**
     * 正则获取sql中所有表名
     *
     * @param sql
     * @return
     */
    public static String getTableNames(String sql) {
        sql = sql.trim().toLowerCase();
        Matcher matcher = null;
        //SELECT 列名称 FROM 表名称
        //SELECT * FROM 表名称
        if (sql.startsWith("select")) {
            matcher = Pattern.compile("select\\s.+from\\s(.+)where\\s(.*)").matcher(sql);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        //INSERT INTO 表名称 VALUES (值1, 值2,....)
        //INSERT INTO table_name (列1, 列2,...) VALUES (值1, 值2,....)
        if (sql.startsWith("insert")) {
            matcher = Pattern.compile("insert\\sinto\\s(.+)\\(.*\\)\\s.*").matcher(sql);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        //UPDATE 表名称 SET 列名称 = 新值 WHERE 列名称 = 某值
        if (sql.startsWith("update")) {
            matcher = Pattern.compile("update\\s(.+)set\\s.*").matcher(sql);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        //DELETE FROM 表名称 WHERE 列名称 = 值
        if (sql.startsWith("delete")) {
            matcher = Pattern.compile("delete\\sfrom\\s(.+)where\\s(.*)").matcher(sql);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }
}
