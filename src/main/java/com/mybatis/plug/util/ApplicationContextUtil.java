package com.mybatis.plug.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * spring容器上下文工具类
 * 这个bean要配置在比较靠前的位置
 */
public class ApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        this.context = context;
    }

    public static ApplicationContext getContext() {
        if (null == context) {
            System.err.println("ApplicationContextUtil context null");
        }
        return context;
    }

    public static boolean isStart() {
        return getContext() == null ? false : true;
    }
}
