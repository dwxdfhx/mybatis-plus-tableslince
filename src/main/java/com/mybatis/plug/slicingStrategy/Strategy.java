package com.mybatis.plug.slicingStrategy;

/**
 * 策略接口
 */
public interface Strategy {
    /**
     * 传入一个需要分表的表名，返回一个处理后的表名
     *
     * @param tableName
     * @return
     */
    String returnTableName(String tableName);

    void run(String tableNameBase, String tableNameNew);
}