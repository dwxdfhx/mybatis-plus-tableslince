package com.mybatis.plug.slicingStrategy;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 按小时切割表策略
 */
public class ByDayStrategy extends AbstratStrategy {
    @Override
    public String returnTableName(String tableName) {
        String tableNameBase = tableName;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
        StringBuilder sb = new StringBuilder(tableName);
        sb.append("_");
        sb.append(sdf.format(new Date()));
        String tableNameNew = sb.toString();
        //执行父类方法
        super.run(tableNameBase, tableNameNew);
        return tableNameNew;
    }
}