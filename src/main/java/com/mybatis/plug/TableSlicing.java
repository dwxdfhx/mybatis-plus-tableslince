package com.mybatis.plug;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * table切割分表注解
 *
 * @author admin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TableSlicing {

    //默认开启分表
    boolean slicing() default true;

    //table名称
    String value();

    String strategy();
}